import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private hc:HttpClient,private router:Router){}
  loginData(form : NgForm){
    if(form.invalid){
      return;
    }
    console.log(form.value)

    this.hc.post('/user/login',form.value).subscribe((response)=>{
      console.log(response)
      alert(response["message"])
      if(response["message"]="logged in"){
        this.router.navigate(['./blog'])
      }
    })
    form.resetForm()
  }
  register(form : NgForm){
    if(form.invalid){
      return;
    }
    this.hc.post('/user/register',form.value).subscribe((response)=>{
    console.log(response)
    alert("user created successfully")
    })
  
    form.resetForm()
  }
}
