//importing express module
const exp=require("express")
const userApp=exp.Router()

//importing mongodb module
const mc=require("mongodb").MongoClient

//import bcrypt module
const bcrypt=require("bcrypt")

//import jwt
const jwt=require("jsonwebtoken") 

//Get the dbUrl
const dbUrl="mongodb+srv://saikeerthi:sairam009@cluster0.gib8t.mongodb.net/meanapp?retryWrites=true&w=majority"

//conneting to db using dbUrl
var dbo;
mc.connect(dbUrl,{useNewUrlParser:true,useUnifiedTopology:true},(error,client)=>{
    if(error){
        console.log("error in db connection")
    }
    else{
        dbo=client.db("meanapp")
        console.log("connected to db")
    }
})

//POST req handler for register
//parse the object
userApp.use(exp.json())
userApp.post('/register',(req,res)=>{
    dbo.collection("usercollection").findOne({email:req.body.email},(error,userobj)=>{
        if(error){
            console.log("error in finding",error)
        }
        else if(userobj==null){
            console.log(userobj)
            let hashedpassword=bcrypt.hashSync(req.body.password,6)
            req.body.password=hashedpassword
            dbo.collection("usercollection").insertOne(req.body,(error,result)=>{
                if(error){
                    console.log("error in creating user obj",error)
                }
                else{
                    res.send({message:"user created"})
                }
            })
        }
        else{
            res.send({message:"user already existed"})
        }
    })
})

//POST req handler for login
userApp.post('/login',(req,res)=>{
    console.log(req.body)
    dbo.collection("usercollection").findOne({email:req.body.email},(error,userobj)=>{
        if(error){
            console.log("error in finding")
        }
        else if(userobj==null){
            res.send({message:"invalid email"})
        }
        else{
            //compare password
            bcrypt.compare(req.body.password,userobj.password,(error,result)=>{
                if(error){
                    console.log("error in comparing password")
                }
                else if(result==false){
                    res.send({message:"invalid password"})
                }
                else{
                    jwt.sign({email:userobj.email},'skkk',{expiresIn:60},(error,signedToken)=>{
                        if(error){
                            console.log("error in creating token",error)
                        }
                        else{
                            res.send({message:"logged in",token:signedToken,userdetails:userobj})
                        }
                    })
                }
            })
        }
    })
})

//exporting userApp to server
module.exports=userApp