//importing express module
const exp=require("express")
const app=exp();

//importing apis
const userApi=require("./apis/restApi")

//reqs forwarding logic
app.use("/user",userApi)

//importing path module
const path=require("path")
app.use(exp.static(path.join(__dirname,'./dist/meanapp')))

//assigning port number
const port=9000;
app.listen(port,()=>{
    console.log("server is running on",port)
})